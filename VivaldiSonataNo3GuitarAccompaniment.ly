\header {
	title =		"Sonata No.3"
	composer =	"Antonio Vivaldi"
	instrument =	"Guitar (accompaniment)"

}

\version "2.18.2-1"
#(set-global-staff-size 20)

global =  {
	\clef "treble_8"
	\key a \minor
	\time 3/4
	%\override Score.BarNumber.break-visibility = ##(#t #t #t)
}

melody =  \relative c' {
	\global
	\voiceOne

	\repeat volta 2 {
	% 1
	a'4 a,8. c16 b8. a16 
	e'4 e, r 
	e' f fis8 c
	e b e fis gis4

	% 5
	e,2 e8 b'
	c4. b8 a4
	c2 d8 a
	g4. a8 b4

	%9
	c4 c8. d16 e8. c16
	g'4 g,4 r
	g'2 <g b>4
	c a <g b>

	%13
	g~ g8 c4 b8
	<g c>2 <g c,>4
	<aes c,> <g d> f
	<d f>2 <c e!>4
	}

	\repeat volta 2 {
	%16
	r8 g c4 a
	b d8. c16 b4
	r8 b d4 b
	c e8. d16 c4
	

	%20
	d a8. g16 f4
	g d'8. c16 bes4~
	bes bes'8. a16 g4~
	g4. cis,8 d e

	%24
	d bes a4 cis
	d2 r4
	a'4 a,8. c16 b8. a16 
	e'4 e, r
	
	%28
	a4. b8 c4~
	c8. b16 a8. g16 f8. e16
	b'4. c8 d4~
	d8. c16 b8. a16 gis8. fis16

	%33
	d'4. e8 fis gis
	a8. f!16 e8. d16 c4~
	c8 b c4 d
	b8 b e gis a c, 

	%37
	d f e4 d8 c
	b2 e4~
	e8 c a'4 gis8 a
	<c, e a>2. \fermata

	%40
	}
}

bass =  \relative c {
	\global
	\voiceTwo

	% 1
	a'4 a,8. c16 b8. a16 
	e'4 e, r 
	c' d dis
	e2 e4

	%5
	gis,2.
	a2 a4
	f fis2
	g2.

	%9
	c4 c8. d16 e8. c16
	g'4 g,4 r
	b' c d
	c fis, g

	%13
	b c d 
	ees2 ees,4
	f g g,
	c2.

	%16
	e2 fis4
	g2 g,4
	g'2 gis4
	a2 a,4

	%20
	d2.
	g,2.
	g
	a4 a'8 g f cis

	%24
	d g a4 a,
	d2 r4
	a'4 a,8. c16 b8. a16 
	e'4 e, r
	
	%28
	c'2.
	d2.
	d4. e8 f4
	e2.

	%32
	e
	a,
	f'4 e d
	e2

	%36
	a,4
	d e e,
	gis2.
	a4 e' e,

	%40
	a2. 

}

GuitarStaff = \context Staff <<
	\set Staff.midiInstrument = "acoustic guitar (nylon)"
	% \transposition c 

	\melody
	\bass
>>

\score {
	\GuitarStaff

	
  \midi {
    \tempo 4 = 80
    }



	\layout {
	}
}